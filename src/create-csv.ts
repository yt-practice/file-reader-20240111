import { writeFile } from 'fs/promises'
import { join } from 'path'

// create csv files
;(async () => {
  for (const name of ['bad', 'good'] as const) {
    const isBad = 'bad' === name
    const nums = (length: number) => Array.from({ length }, (_, i) => i)
    const result = nums(100000)
      .map(() =>
        nums(100)
          .map(j => (isBad ? 'bad' : `${j}`))
          .join(','),
      )
      .join('\r\n')
    await writeFile(join(__dirname, '..', `${name}.csv`), result)
  }
})().catch(x => {
  console.error(x)
  process.exit(1)
})
