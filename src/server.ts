import { createServer } from 'http'

createServer(async (req, res) => {
  if ('/api/upload' === req.url) {
    const buffers: Buffer[] = []
    for await (const chunk of req) {
      buffers.push(chunk)
    }
    const buffer = Buffer.concat(buffers)
    /** @todo エンコードを考慮 */
    console.log('ok', buffer.length, buffer.toString().slice(0, 100))
    res.end('done!')
    return
  }
  res.end('unknown route')
}).listen(3000, () => console.log('Server is running'))
