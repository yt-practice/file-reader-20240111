/* eslint-disable import/no-deprecated */
import { h, render } from 'preact'
import { useReducer } from 'preact/hooks'

/**
 * RFC 4180 に従って CSV 形式の文字列をパースし、各行を文字列の配列として返す。
 *
 * @param d CSV 形式の文字列
 */
const parseCSV = function* (d: string) {
  let t = d.replace(/(\r?\n|\r)$/u, '')
  let r: string[] = []
  let l: string
  let m: string
  let n: string
  type E = readonly [string, string, string]
  const reg = /^(|[^",][^,\r\n]*|"(?:[^"]|"")*")(,|\r?\n|\r|$)/u
  while (([l, m, n] = reg.exec(t) as unknown as E)) {
    if ('"' === m[0] && '"' === m[m.length - 1]) m = m.slice(1, -1)
    r.push(m.replace(/""/gu, '"'))
    if (!n) break
    if (',' !== n) {
      yield r
      r = []
    }
    t = t.substring(l.length)
  }
}

let err = ''
let color: 'blue' | 'red' = 'blue'

/**
 * ファイル選択時のイベントハンドラ。
 * ファイルが選択されると、そのファイルを読み込み、その内容を CSV として1行目のみパースする。
 * パース結果が空の場合は、エラーを出力する。
 * パース結果に重複がある場合は、エラーを出力する。
 * パース結果が正常な場合は ok を出力する。
 *
 * @param e イベント
 */
const onChangeFile = (e: { target: EventTarget | null }) => {
  if (e.target instanceof HTMLInputElement && e.target.files) {
    for (const file of Array.from(e.target.files)) {
      if (!/^text\/csv$/iu.test(file.type)) continue
      const reader = new FileReader()
      reader.onload = () => {
        color = 'red'
        const text = reader.result as string
        const ret: string[] = []
        /** @todo エンコードを考慮 */
        for (const line of parseCSV(text)) {
          if (line.every((v, i, a) => i === a.indexOf(v))) {
            ret.push('ok')
            color = 'blue'
          } else {
            console.log('duplicate', line)
            ret.push('duplicate')
          }
          break
        }
        console.log('load', file.name, text.length)
        if (!ret.length) ret.push('empty')
        ret.unshift(`loaded ${file.name}, ${text.length}`)
        err = ret.join('\n')
        pin()
      }
      reader.readAsText(file)
    }
  }
}

let pin: () => void = () => {}

const App = () => {
  pin = useReducer<boolean, void>(s => !s, false)[1]
  return h(
    'div',
    {},
    h(
      'form',
      {
        action: 'http://localhost:3000/api/upload',
        method: 'POST',
        enctype: 'multipart/form-data',
      },
      h('input', {
        type: 'file',
        accept: '.csv',
        name: 'direct_sale_csv',
        onChange: onChangeFile,
      }),
      h('button', { type: 'submit' }, 'Submit'),
    ),
    !err ? null : h('pre', { style: { color } }, err),
  )
}

render(h(App, {}), document.body)
